FROM osixia/openldap:1.5.0
LABEL maintainer="simon.reis@pta.de"

COPY sevenseas-example-data.ldif /container/service/slapd/assets/config/bootstrap/ldif/50-bootstrap.ldif